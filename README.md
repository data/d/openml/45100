# OpenML dataset: SMK

https://www.openml.org/d/45100

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**SMK (SMK-CAN-187) dataset**

**Authors**: A. Spira, J. Beane, V. Shah, K. Steiling, G. Liu, F. Schembri, S. Gilman, Y. Dumas, P. Calner, P. Sebastiani, et al

**Please cite**: ([URL](https://www.nature.com/articles/nm1556)): A. Spira, J. Beane, V. Shah, K. Steiling, G. Liu, F. Schembri, S. Gilman, Y. Dumas, P. Calner, P. Sebastiani, et al, Airway epithelial gene expression in the diagnostic evaluation of smokers with suspect lung cancer, Nat. Med. 13 (3) (2007) 361-366.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45100) of an [OpenML dataset](https://www.openml.org/d/45100). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45100/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45100/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45100/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

